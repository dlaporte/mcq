<?php
$MSG_NAME      = "Nom";
$MSG_FNAME     = "Prénom";
$MSG_UNKNOWN   = "Utilisateur inconnu";
$MSG_CONNECT   = "Se connecter";
$MSG_NEW       = "Nouveau";
$MSG_ISOK      = "Enregistrement OK";
$MSG_ERRUPDATE = "Erreur mise à jour";
$MSG_ERRAPPEND = "Erreur ajout";
$MSG_MCQ       = "QCM";
$MSG_QUESTIONS = "Questions";
$MSG_NEXT      = "Suivant";
$MSG_PREV      = "Précédent";
$MSG_END       = "FIN";
$MSG_IMPORT    = "Importer QCM";
$MSG_ERRJSON   = "Erreur création %1 json";
$MSG_IMPORTOK  = "Création QCM %1 : %2 questions";
$MSG_ADDUSR    = "Importer des utilisateurs";
$MSG_ERRIMPORT = "Erreur importation %1";
$MSG_IMPORTUSR = "Importation %1 : %2 utilisateurs";
$MSG_GROUP     = "groupe %1";
?>