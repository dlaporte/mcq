====Architecture des ordinateurs

Question 1
Une carte m�re sert � :

x D�terminer les caract�ristiques d�un micro-ordinateur
x Lier tous les composants du PC, de la m�moire aux cartes d�extension, en passant par les lecteurs de disques
Connecter plusieurs ordinateurs en r�seau autour d�une unit� centrale appel�e encore unit� m�re
Question 2
La carte m�re d�termine le type des autres composants.

x Vrai
Faux
Question 3
Cochez parmi ces normes celles qui repr�sentent un format de carte m�re :

NLX
PCI
x AT
x ATX
USB
EISA
Question 4
Il est possible de modifier la fr�quence d�une carte m�re par :

Un jumper (cavalier)
Une carte d�extension
Une barrette de m�moire suppl�mentaire
Question 5
A quoi sert la pile situ�e sur la carte m�re ?

A maintenir le BIOS sous tension de mani�re permanente
A conserver le param�trage du BIOS
A conserver la liste de tous les mots de passe utilis�s par l�ordinateur
Question 6
Que signifie le terme ACPI ?

Auto Configurating Power Interface.
Acc�s Central des Programmes Install�s.
Advanced Configuration and Power Interface.
Auto Configuration of Power Interface.
Question 7
A quoi sert l�ACPI ?

Il permet de r�duire la consommation �lectrique du PC.
Il permet un meilleur contr�le de l��nergie par le syst�me d�exploitation.
Il permet de d�finir un point d�acc�s unique pour tous les utilisateurs du PC.
Il permet au PC de revenir � la vie instantan�ment et r�duit le bruit lorsque le PC n�est pas utilis�.
Question 8
Qu�est-ce qu�un bus ?

C�est un c�ble qui relie le lecteur de CD � la carte m�re.
C�est la nappe sur laquelle est connect� le lecteur de disquette.
C�est l�ensemble des liaisons �lectroniques permettant la circulation des donn�es entre le processeur, la m�moire vive et les cartes d�extension.

==== Num�ration et codage

Question 1/9 : Changement de base

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Que donne le nombre hexad�cimal 0x3F en base d�cimale ?

Je ne sais pas
A) 18
xB) 63
C) 77


Question 2/9 : Changement de base

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Que donne le nombre d�cimal 36 en base hexad�cimale ?

Je ne sais pas
xA) 0x24
B) 0x44
C) 0x54


Question 3/9 : Changement de base

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Que donne le nombre hexad�cimal 0xA7 en base binaire ?

Je ne sais pas
xA) 0b10100111
B) 0b10110011
C) 0b10010111


Question 4/9 :

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Quelle est la proposition exacte ?

Je ne sais pas
A) 1 octet = 8 bytes = 8 bits
B) 1 bit = 1 byte = 8 octets
xC) 1 byte = 1 octet = 8 bits


Question 5/9 :

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


On s'int�resse � l'octet 0x2C.

Que valent le bit de poids fort (MSB) et le bit de poids faible (LSB) ?

Je ne sais pas
xA) MSB = 0 ; LSB = 0
B) MSB = 0 ; LSB = 1
C) MSB = 1 ; LSB = 0
D) MSB = 1 ; LSB = 1


Question 6/9 :

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Combien de valeurs peut-on coder avec un nombre binaire de 16 bits ?

Je ne sais pas
A) 4
B) 256
xC) 65536


Question 7/9 :

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Combien de bits faut-il pour coder un nombre entier compris entre -100 et +100 ?

Je ne sais pas
A) 7 bits minimum
xB) 8 bits minimum
C) 10 bits minimum


Question 8/9 :

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Combien d'octets faut-il pour coder un pixel d'une image en 256 niveaux de gris ?

Je ne sais pas
xA) 1 octet
B) 2 octets
C) 8 octets


Question 9/9 : Code ASCII

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


Le code ASCII de la lettre A est 0x41, 0x42 pour B, 0x43 pour C...

Quel est le code ASCII de la 26�me et derni�re lettre de l'alphabet Z ?

Je ne sais pas
A) 0x66
B) 0x5B
xC) 0x5A


====Langages de programmation


1
Quelle proposition d�signe un langage de programmation ?
   A
   B
   C
2
Je suis un langage utilis� principalement en math�matiques et pour les sciences :
   Fortran
   AppleScript
   PHP
3
Parmis les propositions suivantes, quel langage est tr�s utilis� pour les pages web ?
   COBOL
   Javascript
   SPL
Quizz.biz est financ� par la publicit�, celle-ci permet de vous offrir du contenu gratuitement.
Merci de d�sactiver votre Adblock, ou de voir comment nous aider
en nous contactant � julien@quizz.biz
4
J'utilise la logique polonaise invers�e et la notion de pile :
   C ++
   Forth
   Vala
5
J'ai �t� cr�� par Microsoft :
   Shell Unix
   Visual Basic
   Python
6
Je suis un langage de programmation orient� objet :
   Java
   Pascal
   L'assembleur
7
Quel langage est le pr�curseur de Delphi?
   C ++
   Pascal
   Perl
8
Parmis ces propositions laquelle n'est pas un langage de programmation ?
   Caml
   RedHat
   Eiffel
9
Tout comme il existe le C++, il existe le C -- .
   Vrai
   Faux
10
Plus difficile: quel est le langage de programmation de la calculatrice HP49 ?
   Ti-basic
   MLC
   RPL


=====Langages HTML/CSS


1

Salut !
affiche ...
   'Salut ! ' en gras
   'Salut ! ' en italique
   'Salut ! ' normlement
   Rien
2
En langage HTML, comment fait-on un passage � la ligne ?
   On appuie sur la touche 'Entr�e'
   On �crit ''
   On �crit '
'
3
En CSS, quand je met '. titre', cela concerne ...
   La class intitul� 'titre'
   L'id intitul� 'titre'
   Rien
Quizz.biz est financ� par la publicit�, celle-ci permet de vous offrir du contenu gratuitement.
Merci de d�sactiver votre Adblock, ou de voir comment nous aider
en nous contactant � julien@quizz.biz
4
En CSS, quand je met '#titre em', cela concerne ...
   Tous les mots entour�s par
   Les mots entour�s par dans l'id 'titre'
   Les mots entour�s par dans la class 'titre'
5
En HTML, la page commence par ...
   Une balise de type body
   Une balise de type doctype
   Une balise de type html
   Une balise de type h1
6
En HTML, il est possible de faire des questionnaires.
   Vrai
   Faux
7
En CSS, On met le code ...
   Entre []
   Entre ()
   Entre {}
8
Quelle balise permet de mettre du code CSS sur une page HTML ?
   
   

====Langage de programmation Python

==Les variables (Python 3)
Question 1/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,25 point, je ne sais pas 0 point


a = 12
b = 8
c = a
a = b
b = c
print(a,b)

Qu'affiche le script ?

Je ne sais pas
A) 8 8
B) 8 12
C) 12 8
D) 12 12


Question 2/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,25 point, je ne sais pas 0 point


toto = 1+3/5
print(toto)

Qu'affiche le script ?

Je ne sais pas
A) 0.8
B) 1.0
C) 1
D) 1.6


Question 3/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,25 point, je ne sais pas 0 point


toto = 1.0+3.0/5.0
print(toto)

Qu'affiche le script ?

Je ne sais pas
A) 0.8
B) 1.0
C) 1
D) 1.6


Question 4/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


toto = 13.4
Toto = 10.5
Toto = Toto -5.5
print(toto)

Qu'affiche le script ?

Je ne sais pas
A) 5.0
B) 10.5
C) 13.4


Question 5/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a = '10'
b = '2'
c = a + b
print(c)

Qu'affiche le script ?

Je ne sais pas
A) 12
B) 102
C) 210


Question 6/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a = 12e3
print(type(a))

Qu'affiche le script ?

Je ne sais pas
A) <class 'int'>
B) <class 'float'>
C) <class 'str'>


Question 7/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a = "4*5"
print(a)

Qu'affiche le script ?

Je ne sais pas
A) 4*5
B) 20
C) 20.0


Question 8/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a = 2,5
b = 1,8
print(a*b)

Qu'affiche le script ?

Je ne sais pas
A) 4,5
B) 4.5
C) Le script g�n�re une erreur.


Question 9/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a="Bon"
b="jour"
print(b+a)

Qu'affiche le script ?

Je ne sais pas
A) jourBon
B) Bonjour
C) b+a


Question 10/10 : Python

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


toto = 'Bonsoir'
print(toto[3:7])

Qu'affiche le script ?

Je ne sais pas
A) toto[3:7]
B) soir
C) nsoir

==Les structures conditionnelles
Question 1/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a=7
b=12
if a>5:
    b=b-4
if b>=10:
    b=b+1

Que vaut la valeur finale de la variable b ?

Je ne sais pas
A) 8
B) 9
C) 12
D) 13


Question 2/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


a=3
b=6
if a>5 or b!=3:
    b=4
else:
    b=2

Que vaut la valeur finale de la variable b ?

Je ne sais pas
A) 2
B) 4
C) 6


Question 3/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


a=2
b=5
if a>8:
    b=10
elif a>6:
    b=3

Que vaut la valeur finale de la variable b ?

Je ne sais pas
A) 3
B) 5
C) 10


Question 4/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a=2
b=0
if a<0:
    b=1
elif a>0 and a<5:
    b=2
else:
    b=3

Que vaut la valeur finale de la variable b ?

Je ne sais pas
A) 0
B) 1
C) 2
D) 3


Question 5/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


a=10
if a<5:
    a=20
elif a<100:
    a=500
elif a<1000:
    a=1
else:
    a=0

Que vaut la valeur finale de la variable a ?

Je ne sais pas
A) 0
B) 1
C) 10
D) 20
E) 500 

==Les boucles for et while
Question 1/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


n = 0
while n<15 :
    n = n + 2
print(n)

Qu'affiche le script ?

Je ne sais pas
A) 14
B) 15
C) 16
D) 17


Question 2/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


n = 10
while n>=11 :
    n = n + 2
print(n)

Qu'affiche le script ?

Je ne sais pas
A) 10
B) 11
C) 12
D) 13


Question 3/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


n = 0
for i in range(5) :
    n = n + 1
print(n)

Qu'affiche le script ?

Je ne sais pas
A) 4
B) 5
C) 6
D) 7


Question 4/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


n = 0
for i in range(5) :
    n = n + 1
print(i)

Qu'affiche le script ?

Je ne sais pas
A) 4
B) 5
C) 6
D) 7


Question 5/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


resultat = ""
for c in "Bonsoir" :
    resultat = resultat + c
print(resultat)

Qu'affiche le script ?

Je ne sais pas
A) Bonsoir
B) riosnoB
C) BonsoirBonsoirBonsoirBonsoirBonsoirBonsoirBonsoir


==Les fonctions
Question 1/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


def func(a):
    a += 2.0
    return a

a = func(8.0)
print(a)

Qu'affiche le script ?

Je ne sais pas
A) 8.0
B) 10.0
C) 12.0


Question 2/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


def diff(val1,val2):
    return val2 - val1

a = diff(3.0,-2.0)
print(a)

Qu'affiche le script ?

Je ne sais pas
A) 5.0
B) 1.0
C) -1.0
D) -5.0


Question 3/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -2 points, je ne sais pas 0 point


def func(val):
    if val<0.0:
        return 0
    return val

a = func(-1.5)
print(a)

Qu'affiche le script ?

Je ne sais pas
A) 0
B) -1.5


Question 4/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


def carre(val):
    return val*val

def inc(val):
    return val + 1

a = carre(inc(3.0))
print(a)

Qu'affiche le script ?

Je ne sais pas
A) 10.0
B) 12.0
C) 16.0


Question 5/5 : Python

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


def func(a):
    a += 2.0
    return a

a = 5.0
b = func(a)
print(a,b)

Qu'affiche le script ?

Je ne sais pas
A) 5.0 5.0
B) 5.0 7.0
C) 7.0 5.0
D) 7.0 7.0


=====Langage de programmation PHP

==Les variables
Question 1/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


<?php
$res = 2+3/4;
print $res;
?>

Qu'affiche le script ?

Je ne sais pas
A) 1.25
B) 2
C) 2.75


Question 2/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


<?php
$res = 15;
$Res = 10;
$Res = $Res -7;
print $res;
?>

Qu'affiche le script ?

Je ne sais pas
A) 3
B) 8
C) 15


Question 3/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


<?php
$a = 6;
$b = 15;
$c = $a;
$a = $b;
$b = $c;
print "$a $b";
?>

Qu'affiche le script ?

Je ne sais pas
A) 6 6
B) 6 15
C) 15 6
D) 15 15


Question 4/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


<?php
$a = 1,5;
$b = 2,2;
$c = $a + $b;
print $c;
?>

Qu'affiche le script ?

Je ne sais pas
A) 3,7
B) 3.7
C) Le script g�n�re une erreur.


Question 5/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


<?php
$a = "3*7";
print $a;
?>

Qu'affiche le script ?

Je ne sais pas
A) 21
B) 3*7


Question 6/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


<?php
$a = "4";
$b = "2";
$c = $a.$b;
print $c;
?>

Qu'affiche le script ?

Je ne sais pas
A) 8
B) 24
C) 42


Question 7/7 : PHP

Bar�me : bonne r�ponse 2 points, mauvaise r�ponse -0,5 point, je ne sais pas 0 point


<?php
$a = "Bon";
$b = "jour";
print $b.$a;
?>

Qu'affiche le script ?

Je ne sais pas
A) jourBon
B) Bonjour
C) $b.$a

==Les structures conditionnelles
Question 1/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$a=7;
$b=12;
if ($a>5) {
    $b=$b-4;
}
if ($b>=10) {
    $b=$b+1;
}
print $b;

Qu'affiche le script ?

Je ne sais pas
A) 8
B) 9
C) 12
D) 13


Question 2/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$a=3;
$b=6;
if ($a>5 || $b!=3) {
    $b=4;
} else {
    $b=2;
}
print $b;

Qu'affiche le script ?

Je ne sais pas
A) 2
B) 4
C) 6


Question 3/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$a=2;
$b=5;
if ($a>8) {
    $b=10;
} elseif ($a>6) {
    $b=3;
}
print $b;

Qu'affiche le script ?

Je ne sais pas
A) 3
B) 5
C) 10


Question 4/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$a=2;
$b=0;
if ($a<0) {
    $b=1;
} elseif ($a>0 && $a<5) {
    $b=2;
} else {
    $b=3;
}
print $b;

Qu'affiche le script ?

Je ne sais pas
A) 0
B) 1
C) 2
D) 3


Question 5/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$a=10;
if ($a<5) {
    $a=20;
} elseif ($a<100) {
    $a=500;
} elseif ($a<1000) {
    $a=1;
} else {
    $a=0;
}
print $a;

Qu'affiche le script ?

Je ne sais pas
A) 0
B) 1
C) 10
D) 20
E) 500

Question 1/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$n = 0;
while ($n<15) {
    $n = $n + 2;
}
print $n;

Qu'affiche le script ?

Je ne sais pas
A) 14
B) 15
C) 16
D) 17


Question 2/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$n = 10;
while ($n>=11) {
    $n = $n + 2;
}
print $n;

Qu'affiche le script ?

Je ne sais pas
A) 10
B) 11
C) 12
D) 13


Question 3/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$n = 0;
for ($i=0;$i<=4;$i++) {
    $n = $n + 1;
}
print $n;

Qu'affiche le script ?

Je ne sais pas
A) 4
B) 5
C) 6
D) 7


Question 4/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$n = 0;
for ($i=0;$i<5;$i++) {
    $n = $n + 1;
}
print $i;

Qu'affiche le script ?

Je ne sais pas
A) 4
B) 5
C) 6
D) 7


Question 5/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


$resultat = "Bonjour";
for ($i=1;$i<3;$i++) {
    $resultat = $resultat."Bonsoir";
}
print $resultat;

Qu'affiche le script ?

Je ne sais pas
A) BonjourBonsoir
B) BonjourBonsoirBonsoir
C) BonsoirBonsoirBonjour


==Les fonctions
Question 1/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


function func($a)
{
    $a += 2;
    return $a;
}

$a = func(8);
print $a;

Qu'affiche le script ?

Je ne sais pas
A) 8
B) 10
C) 12


Question 2/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


function diff($val1,$val2)
{
    return $val2 - $val1;
}

$a = diff(3,-2);
print $a;

Qu'affiche le script ?

Je ne sais pas
A) 5
B) 1
C) -1
D) -5


Question 3/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -2 points, je ne sais pas 0 point


function func($val)
{
    if ($val<0.0)return 0;
    return $val;
}

$a = func(-1.5);
print $a;

Qu'affiche le script ?

Je ne sais pas
A) 0
B) -1.5


Question 4/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


function carre($val)
{
    return $val*$val;
}

function inc($val)
{
    return $val + 1;
}

$a = carre(inc(3));
print $a;

Qu'affiche le script ?

Je ne sais pas
A) 10
B) 12
C) 16


Question 5/5 : PHP

Bar�me : bonne r�ponse 4 points, mauvaise r�ponse -1 point, je ne sais pas 0 point


function func($a)
{
    $a += 2;
    return $a;
}

$a = 5;
$b = func($a);
print "$a $b";

Qu'affiche le script ?

Je ne sais pas
A) 5 5
B) 5 7
C) 7 5
D) 7 7


=====Informatique et libert�s

==Internet et le droit

    Publier sur Internet
    Droit � l'image et propri�t� intellectuelle

======S�curit� informatique

1
En entreprise, quels sont les principaux facteurs de risque en s�curit� informatique ?
   D�faillance du mat�riel
x   Erreurs humaines et mauvais comportement
   Les �v�nements naturels
   Panne d'�lectricit�
2
La tentative d'infraction informatique est-elle condamn�e en France?
x   Oui, par les m�mes peines que l'infraction
   Oui, par des peines moindres
   Non, la loi ne pr�cise pas
3
Est-ce que cette affirmation est correcte: �Les virus existent seulement sous Windows, les autres syst�mes d'exploitation et les PDA ne sont pas touch�s?�
   Vrai
x   Faux
Quizz.biz est financ� par la publicit�, celle-ci permet de vous offrir du contenu gratuitement.
Merci de d�sactiver votre Adblock, ou de voir comment nous aider
en nous contactant � julien@quizz.biz
4
Si je proc�de � des activit�s peu s�res sur mon ordinateur et que j'installe moi-m�me des logiciels, je suis le seul � courir un risque.
   Vrai
x   Faux
5
Lorsque je suis en d�placement � l'ext�rieur de l'entreprise, comment puis-je continuer � participer � la s�curit� de mon entreprise ?
   En d�sactivant l'anti-virus, pour ne pas consommer
   En installant un antivirus plus efficace
   En r�alisant moi-m�me des � tests d'intrusions � sur mon entreprise
x   En utilisant mon poste de travail � l'abri des regards indiscrets, et en n'installant aucun logiciel non autoris� sur mon poste
6
Je viens de recevoir un message m'alertant qu'un nouveau virus ultra destructeur venait d'appara�tre, message issu semble-t-il d'un leader de l'informatique et d'un leader de la s�curit� antivirus.
x   C'est un canular et je d�truis ce message
   C'est un canular et je pr�viens le plus de gens possible que c'est un canular
   Je coupe mon poste et je pleure sur mon pc perdu
7
Vous recevez un e-mail en anglais d'une personne que vous ne connaissez pas...
   Vous le lisez, vous n'ouvrez pas la pi�ce jointe mais vous cliquez sur un lien
   Vous le lisez et vous ouvrez la pi�ce jointe (y'a pas de risque!)
x   Vous le supprimez directement
8
De quoi est capable un virus informatique ?
   Modifier le fonctionnement de votre ordinateur
   Vous envoyer des messages �lectroniques frauduleux et en envoyer � partir de votre ordinateur
   Bloquer le syst�me de votre ordinateur et provoquer de red�marrages intempestifs
x   Toutes les r�ponses ci-dessus
9
La messagerie instantan�e est un moyen extraordinaire de discuter avec des amis sur Internet sans s'exposer � des virus.
   Vrai
x   Faux
10
Vous �tes abonn� eBay, vous recevez un de leur mail avec un lien disant de retaper vos coordonn�es bancaires suite � probl�me...
   Vous faites ce qu'il dit en cliquant sur le lien
   Vous ne faites rien
x   Vous vous rendez sur ebay.fr afin de v�rifier
