<?php
/**
    @file   scores.php
    @brief  calcul et maj des scores obtenus au QCM

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

//---------------------------------------------------------------------------
function update_scores($IDquestion, $checkbox)
{
	global $dal;
	if ( (@$_SESSION["id"] > 1) and !@$_SESSION["readonly"] )
		if ( $dal->insert_score(@$_SESSION["IDmcq"], $IDquestion, array_to_string($checkbox)) == 0 )
			$dal->update_score(@$_SESSION["IDmcq"], $IDquestion, array_to_string($checkbox));
}
//---------------------------------------------------------------------------
function compute_score($mcq, $answer)
{
	global $dal;
	
	$checked = 0;
	$good    = 0;
	for ($i = 0; $i < count($mcq['answers']); $i++) {
		$is_ok = strtolower($mcq['answers'][$i]['check']) == "x";
		if ( $is_ok )
			$checked++;
		if ( in_array($i+1, @explode(",", $answer['answer'])) )
			$good += $is_ok ? 1 : -1 ;
		}
	if ( $good < 0 )
		$good = 0;
	
	return $checked ? $good / $checked : 0 ;
}
//---------------------------------------------------------------------------
function get_score($IDmcq, $file)
{
	global $dal;

	$points = 0.0;
	
	$quizz  = json_decode(file_get_contents("./data/$file.json"), true);	
	$answer = $dal->select_scores_answer($IDmcq);
	$index  = @explode(",", $dal->select_mcq_questions($IDmcq)[0]["questions"]);

	$page = 1;
	foreach ( $index as $i ) {
		$points += compute_score($quizz['questions'][$i-1], @$answer[$page-1]);
		$page++;
		}

	return $page > 1 ? (int) (100 * $points / ($page - 1)) : 0 ;
}
//---------------------------------------------------------------------------
?>