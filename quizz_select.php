<?php
/**
    @file   quizz_select.php
    @brief  affichage de la liste des QCM

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( !isset($_SESSION["id"]) ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./data/msg/$LANG.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("quizz_select.html");

$tpl->setVar('questions', $MSG_QUESTIONS);

$count = 0;
$dir   = "./data";
foreach ( scandir($dir) as $file )
	if( is_file("$dir/$file") and "$dir/$file" != '.' and "$dir/$file" != '..' )
		if( substr(strtolower("$dir/$file"), -5, 5) == ".json" ) {
			$count++;
			$quizz = json_decode(file_get_contents("$dir/$file"), true);			
			$nb_questions = count($quizz['questions']) < $RANDOM ? count($quizz['questions']) : $RANDOM ;
			
			$tpl->setVar('qcm', substr($file, 0, strlen($file) - 5));
			$tpl->setVar('count', @$_SESSION["id"] == 1 ? count($quizz['questions']) : $nb_questions);
			$tpl->render('row');
			}
if ( $count == 0 )
	$tpl->render('empty');

if ( @$_SESSION["id"] == 1 ) {
	$tpl->render('admin');
	$tpl->render('admin_import');
	}
if ( @$_SESSION["id"] )
	$tpl->render('user');

echo $tpl->render();
?>