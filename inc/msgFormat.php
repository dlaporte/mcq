<?php
/**
    @file   msgFormat.php
    @brief  g�n�ration des fichiers messages

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

function msgFormat($IDmsg, $arg = "")
{
	/** formattage d'un message
		@param	$IDmsg -- n� du message
		@param	$arg -- n� argument dans le message
		@return	message texte
	 */

	if ( is_array($arg) )
		for ($i=1; $i <= count($arg); $i++)
			$IDmsg = str_replace("%$i", $arg[$i-1], $IDmsg);
	else if ( $arg != "" )
		return str_replace("%1", $arg, $IDmsg);
	return $IDmsg;
}
?>