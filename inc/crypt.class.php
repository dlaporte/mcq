<?php
/*----------------------------------------------------------------------------*
   This file is part of QDRep. QDRep is a file sharing software.
   Copyleft (c) 2020 by Dominique Laporte(dominique.laporte@ac-aix-marseille.fr)

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*----------------------------------------------------------------------------*/
// SPDX-License-Identifier: AGPL-3.0-or-later
//---------------------------------------------------------------------------
class Mycrypt {
	private $key    = "";			// Clé de chiffrement
	private $cipher = "bf-cbc";		// Algorithme utilisé pour le chiffrement des blocs
	private $iv     = "12345678";	// vecteur d'initalisation

	//-----------------------------------------------------------------------
	public function __construct($key, $cipher="bf-cbc") {
		if ( function_exists("openssl_get_cipher_methods") )
			if ( in_array($cipher, openssl_get_cipher_methods()) ) {
				$this->key    = $key;
				$this->cipher = $cipher;
				}
		}

	//-----------------------------------------------------------------------
	public function get_cipher() {
		return $this->cipher;
		}

	//-----------------------------------------------------------------------
	public function encrypt($plaintext) {
		return $this->key != ""
			? @openssl_encrypt($plaintext, $this->cipher, $this->key, 0, $this->iv)
			: $plaintext ;
		}

	//-----------------------------------------------------------------------
	public function decrypt($ciphertext) {
		return $this->key != ""
			? @openssl_decrypt($ciphertext, $this->cipher, $this->key, 0, $this->iv)
			: $ciphertext ;
		}

	//-----------------------------------------------------------------------
	public function encrypt_file($file, $content, $dir="./") {
		// on supprime les fichiers vides
		return strlen($content)
			? file_put_contents($dir.$file, $this->encrypt($content))
			: @unlink($file) ;
		}

	//-----------------------------------------------------------------------
	public function decrypt_file($file, $dir="./") {
		return $this->decrypt(@file_get_contents($dir.$file));
		}
}
//---------------------------------------------------------------------------
?>