<?php
/**
    @file   dal.class.php
    @brief  classe Database Access Layer

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    couche d'abstraction à la base de données
*/

// SPDX-License-Identifier: GPL-3.0-or-later

class DAL {
    private $dbh = NULL;

    public function __construct($database) {
        try {
			$this->dbh = new PDO("sqlite:$database.sqlite");
			$this->dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} 
		catch(Exception $e) {
			die("Can't connect to '$database': ".$e->getMessage());
			}
		}
	
    public function __destruct() {
        $this->dbh = NULL;
		}
		
    public function close() {
        $this->dbh = NULL;
		}

    public function create_database() {
		try {
			$this->dbh->query(
				"CREATE TABLE if not exists `groups` (
					`IDgroup`	INTEGER PRIMARY KEY,
					`ident`		varchar(10) not NULL,
					CHECK (ident <> ''),
					UNIQUE(ident)
					);"
				);
			$this->dbh->query(
				"CREATE TABLE if not exists `users` (
					`IDuser`	INTEGER PRIMARY KEY,
					`IDgroup`	INTEGER not NULL,
					`name`		varchar(20) not NULL,
					`fname`		varchar(20) not NULL,
					CHECK (name <> ''),
					UNIQUE(IDgroup, name, fname)
					);"
				);
			$this->dbh->query(
				"CREATE TABLE if not exists `mcq` (
					`IDmcq`			INTEGER PRIMARY KEY,
					`IDuser`		INTEGER not NULL,
					`title`			varchar(80) not NULL,
					`date`			DATE not NULL,
					`questions`		varchar(255) not NULL,
					`timer`			INTEGER not NULL,
					CHECK (title <> ''),
					UNIQUE(IDuser, date)
					);"
				);
			$this->dbh->query(
				"CREATE TABLE if not exists `scores` (
					`IDscore`		INTEGER PRIMARY KEY,
					`IDmcq`			INTEGER not NULL,
					`IDquestion`	INTEGER not NULL,
					`answer`		varchar(40) not NULL,
					UNIQUE(IDmcq, IDquestion)
					);"
				);
			} 
		catch(Exception $e) {
			return false;
			}
		return true;
		}

    public function create_default_group() {
		return $this->create_group('****');
		}

    public function create_group($ident) {
		$ident = trim(htmlspecialchars($ident));
		try {
			$this->dbh->query(
				"INSERT INTO groups VALUES(NULL, '$ident');"
				);
			return $this->dbh->lastInsertId();
			}
		catch(Exception $e) {		
			return 0;
			}
		}

	public function select_all_group() {
		return $this->select_group_by_id(0);
		}

	public function select_group_by_id($id) {
		$request  = "SELECT * FROM groups ";
		$request .= $id ? "WHERE IDgroup = '$id' " : "" ;
		$request .= "ORDER BY ident";
        try {
			if ( $this->dbh != NULL )
				if ( ($result = $this->dbh->query($request)) !== false )
					return $result->fetchAll();
			}
		catch(Exception $e) {
			return false;
			}				
		return false;
		}

	public function select_group_by_name($ident) {
		$request  = "SELECT * FROM groups ";
		$request .= "WHERE ident = '$ident'";
        try {
			if ( $this->dbh != NULL )
				if ( ($result = $this->dbh->query($request)) !== false )
					return $result->fetchAll();
			}
		catch(Exception $e) {
			return false;
			}				
		return false;
		}

	public function delete_group($IDgroup) {
		$users = $this->select_all_account($IDgroup);
		foreach ( $users as $u )
			$this->delete_account($u['IDuser']);
			
		$request  = "DELETE FROM groups ";
		$request .= "WHERE IDgroup = '$IDgroup'";
        try {
			if ( $this->dbh != NULL )
				if ( ($result = $this->dbh->query($request)) !== false )
					return $result->fetchAll();
			}
		catch(Exception $e) {
			return false;
			}				
		return false;
		}

    public function create_admin_account() {
		return $this->insert_account('1', 'admin', 'p@sswd');
		}

    public function insert_account($IDgroup, $name, $fname) {
		$name  = trim(htmlspecialchars($name));
		$fname = trim(htmlspecialchars($fname));
		try {
			$this->dbh->query(
				"INSERT INTO users VALUES(NULL, '$IDgroup', '$name', '$fname');"
				);
			return $this->dbh->lastInsertId();
			}
		catch(Exception $e) {
			return 0;
			}
		}
	
	public function select_all_account($IDgroup = 0) {	
        $request  = "SELECT * ";
        $request .= "FROM users ";
        $request .= "WHERE IDuser <> '1' ";
        $request .= $IDgroup ? "AND IDgroup = '$IDgroup' " : "" ;
        $request .= "ORDER BY name, fname";
        if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function select_account($name, $fname) {
		$name  = trim(htmlspecialchars($name));
		$fname = trim(htmlspecialchars($fname));
			
        $request  = "SELECT * FROM users ";
        $request .= "WHERE name = '$name' ";
        $request .= "AND fname = '$fname'";
        if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function select_account_by_id($id) {
		$request  = "SELECT * FROM users ";
        $request .= "WHERE IDuser = '$id'";
        if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function update_account($id, $name, $fname) {
		$name  = trim(htmlspecialchars($name));
		$fname = trim(htmlspecialchars($fname));
		
		$request  = "UPDATE users ";
        $request .= "SET name = '$name', fname = '$fname' ";
        $request .= "WHERE IDuser = '$id'";
        try {
			$this->dbh->query($request);
			return true;
			}
		catch(Exception $e) {
			return false;
			}
		}

	public function delete_account($id) {
		if ( $this->delete_mcq($id) ) {
			$request  = "DELETE FROM users ";
			$request .= "WHERE IDuser = '$id'";
			try {
				$this->dbh->query($request);
				return true;
				} 
			catch(Exception $e) {
				return false;
				}
			}
		return false;
		}

	public function select_mcq($IDuser = 0) {
        $request  = "SELECT * FROM mcq ";
		$request .= $IDuser ? "WHERE IDuser = '$IDuser' " : "" ;
		$request .= "ORDER BY IDmcq DESC ";
		$this->dbh->query($request);
		if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function select_last_mcq($IDuser = 0) {
        $request  = "SELECT * FROM mcq ";
		$request .= $IDuser ? "WHERE IDuser = '$IDuser' " : "" ;
		$request .= "ORDER BY IDmcq DESC ";
		$request .= "LIMIT 1";
		$this->dbh->query($request);
		if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function select_mcq_questions($IDmcq) {
        $request  = "SELECT questions, IDuser FROM mcq ";
		$request .= "WHERE IDmcq = '$IDmcq'";
		$this->dbh->query($request);
		if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function insert_mcq($IDuser, $name, $questions, $timer=0) {
		$request  = "INSERT INTO mcq ";
		$request .= "VALUES(NULL, '$IDuser', '$name', '".date('Y-m-d H:i:s')."', '$questions', '$timer')";
        try {
			$this->dbh->query($request);
			return $this->dbh->lastInsertId();
			} 
		catch(Exception $e) {
			return 0;
			}
		}

	public function delete_mcq($id) {
		$mcq = $this->select_mcq($id);
		foreach ( $mcq as $m )
			$this->delete_scores($m['IDmcq']);

		$request  = "DELETE FROM mcq ";
		$request .= "WHERE IDuser = '$id'";
		try {
			$this->dbh->query($request);
			return true;
			} 
		catch(Exception $e) {
			return false;
			}
		return false;
		}

	public function insert_score($IDmcq, $IDquestion, $answer) {
		$request  = "INSERT INTO scores ";
		$request .= "VALUES(NULL, '$IDmcq', '$IDquestion', '$answer')";		
        try {
			$this->dbh->query($request);
			return $this->dbh->lastInsertId();
			} 
		catch(Exception $e) {
			return 0;
			}
		}

	public function update_score($IDmcq, $IDquestion, $answer) {
		$request  = "UPDATE scores ";
		$request .= "SET answer = '$answer' ";		
		$request .= "WHERE IDmcq = '$IDmcq' AND IDquestion = '$IDquestion'";		
        try {
			$this->dbh->query($request);
			return True;
			} 
		catch(Exception $e) {
			return False;
			}
		}

	public function select_all_scores() {
		$request  = "SELECT DISTINCT users.IDuser, users.name, users.fname ";
		$request .= "FROM users, mcq ";
        $request .= "WHERE users.IDuser > 1 ";
        $request .= "ORDER BY users.name, users.fname";
        if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function select_scores_by_id($id = 0) {
		$request  = "SELECT DISTINCT users.IDuser, users.name, users.fname, mcq.IDmcq, mcq.title, mcq.date ";
		$request .= "FROM users, mcq ";
        $request .= "WHERE users.IDuser > 1 ";
        $request .= "AND mcq.IDuser = users.IDuser ";
        $request .= $id ? "AND users.IDuser = '$id' " : "" ;
        $request .= "ORDER BY users.name, users.fname, mcq.date DESC";
        if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function select_scores_answer($id) {
		$request  = "SELECT IDquestion, answer ";
		$request .= "FROM scores ";
        $request .= "WHERE IDmcq = '$id' ";
        $request .= "ORDER BY IDscore";
        if ( $this->dbh != NULL )
			if ( ($result = $this->dbh->query($request)) !== false )
				return $result->fetchAll();
		return false;
		}

	public function delete_scores($IDmcq) {
		$request  = "DELETE FROM scores ";
        $request .= "WHERE IDmcq = '$IDmcq'";
        try {
			$this->dbh->query($request);
			return true;
			} 
		catch(Exception $e) {
			return false;
			}
		}		
	}
?>