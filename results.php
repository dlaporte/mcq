<?php
/**
    @file   results.php
    @brief  les résultats du QCM par utilisateur

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( !isset($_SESSION["id"]) ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./scores.php";
require "./data/msg/$LANG.php";
require "./inc/msgFormat.php";
require "./inc/dal.class.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
// connexion à la base de données
$dal = new DAL("./data/$DATABASE");
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("results.html");

$tpl->setVar('MCQ', $MSG_MCQ);

if ( @$_SESSION["id"] == 1 ) {
	$IDgroup = isset($_GET["IDgroup"]) ? $_GET["IDgroup"] : 1 ;
	$groups  = $dal->select_all_group();
	foreach ( $groups as $g ) {
		$tpl->setVar('group', $g);
		$users = @$dal->select_all_account($g['IDgroup']);
		if ( $IDgroup == $g['IDgroup'] ) {
			$tpl->setVar('ident', "___".$g['ident']);
			if ( count($users) )
				foreach ( $users as $user ) {
					$tpl->setVar('name', $user["name"]);
					$tpl->setVar('fname', $user["fname"]);

					$scores = $dal->select_scores_by_id($user["IDuser"]);
					$tpl->setVar('count', count($scores));
					foreach ( $scores as $score ) {			
						$tpl->setVar('title', $score["title"]);
						$tpl->setVar('date', $score["date"]);
						$tpl->setVar('idmcq', $score["IDmcq"]);
						$tpl->setVar('score', get_score($score["IDmcq"], $score["title"]));
						$tpl->render('row');
						}

					$tpl->render('score');
					}
			else
				$tpl->render('empty');
			}
		$tpl->setVar('count', count($users));
		$tpl->render('group');
		}		
	}
else {
	$user = $dal->select_scores_by_id(@$_SESSION["id"]);
	
	if ( count($user) ) {
		$tpl->setVar('name', $user[0]["name"]);
		$tpl->setVar('fname', $user[0]["fname"]);
		$tpl->setVar('count', count($user));
		foreach ( $user as $u ) {
			$tpl->setVar('title', $u["title"]);
			$tpl->setVar('date', $u["date"]);
			$tpl->setVar('idmcq', $u["IDmcq"]);
			$tpl->setVar('score', get_score($u["IDmcq"], $u["title"]));
			$tpl->render('row');
			}
		$tpl->render('score');
		}
	else
		$tpl->render('empty');			
	}

if ( @$_SESSION["id"] == 1 ) {
	$tpl->render('admin');
	$tpl->render('admin_drop');
	}
if ( @$_SESSION["id"] )
	$tpl->render('user');

echo $tpl->render();
?>