<?php
/**
    @file   import.php
    @brief  import des questions du QCM

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( @$_SESSION["id"] != 1 ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./data/msg/$LANG.php";
require "./inc/msgFormat.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
function create_database($file)
{
	$questions = array();
	$answers   = array();
	$item      = array();
	$nb_question = 0;
	
	$dir  = "./data/mcq";
	$data = explode("\n", file_get_contents("$dir/$file.csv"));
	foreach ($data as $items) {
		$item = explode("\t", $items);
		if ( count($item) >= 4 ) {
			if ( strlen(trim($item[0])) ) {
				if ( $nb_question == 0 ) {
					// initialisation 1ère question
					$question  = htmlspecialchars(trim($item[0]));
					$answers[] = array(
						'text'  => htmlspecialchars(trim($item[2])),
						'check' => htmlspecialchars(trim($item[3]))
						);
					$nb_question++;
					}
				else {
					// ajout question
					$questions[] = array(
						'question' => $question,
						'answers'  => $answers,
						'type'     => trim($item[1])
						);
						
					//-- pour debug en cas de pb
					if ( $item[2] == NULL )
						echo $item[0]."<br/>";
					
					$question  = htmlspecialchars(trim($item[0]));
					$answers   = array();
					$answers[] = array(
						'text'  => htmlspecialchars(trim($item[2])),
						'check' => htmlspecialchars(trim($item[3]))
						);
					$nb_question++;
					}
				}
			else {
				$answers[] = array(
					'text'  => htmlspecialchars(trim($item[2])),
					'check' => htmlspecialchars(trim($item[3]))
					);
				}
			}
		else
			$questions[] = array(
				'question' => $question,
				'answers'  => $answers,
				'type'     => @trim($item[1])
				);		
		}

	$json = array(
		'questions' => $questions
		);

	return file_put_contents("./data/$file.json", json_encode($json)) === false
		? -1
		: $nb_question ;
}
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("import.html");

$tpl->setVar('action', "import");
$tpl->setVar('import', $MSG_IMPORT);
$tpl->setVar('display', "none");

if ( @$_POST["submit"] == "new" and @$_POST["checkbox"] ) {
	foreach ( @$_POST["checkbox"] as $file ) {
		$nb_question = create_database($file);

		if ( $nb_question < 0 ) {
			$tpl->setVar('icon', "block");
			$tpl->setVar('color', "red");
			$tpl->setVar('status', msgFormat($MSG_ERRJSON, $file));
			}
		else {
			$tpl->setVar('icon', "thumb_up");
			$tpl->setVar('color', "green");
			$tpl->setVar('status', msgFormat($MSG_IMPORTOK, Array($file, $nb_question)));
			}

		$tpl->render('qcm');
		}
	$tpl->render('import');
	}
else {
	$count = 0;

	$dir   = "./data/mcq";
	foreach ( scandir($dir) as $file ) {
		if( is_file("$dir/$file") and "$dir/$file" != '.' and "$dir/$file" != '..' )
			if( substr(strtolower("$dir/$file"), -4, 4) == ".csv" ) {
				$count++;
				$tpl->setVar('file', $file);
				$tpl->setVar('value', substr($file, 0, strlen($file) - 4));
				$tpl->render('csvfile');
				}
		}
		
	if ( $count == 0 )
		$tpl->render('empty');
	}

if ( @$_SESSION["id"] == 1 )
	$tpl->render('admin');
if ( @$_SESSION["id"] )
	$tpl->render('user');

echo $tpl->render();
?>
