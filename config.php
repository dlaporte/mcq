<?php
/**
    @file   config.php
    @brief  d�finition des variables d'initialisation

    @version   1.0
    @author    D. Laporte
    @date      04/12/2021
    @remark
*/

// SPDX-License-Identifier: GPL-3.0-or-later

//---------------------------------------------------------------------------
static $DATABASE = "database";		// nom de la base de donn�es SQLite
static $RANDOM   = 20;				// nombre de questions al�atoires
static $LANG     = "fr";			// langue IHM par d�faut
static $TIMER    = 0;				// temps pour QCM (0 = illimit�)
//---------------------------------------------------------------------------
?>
