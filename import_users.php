<?php
/**
    @file   import_users.php
    @brief  import des utilisateurs

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( @$_SESSION["id"] != 1 ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./data/msg/$LANG.php";
require "./inc/msgFormat.php";
require "./inc/dal.class.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
function create_database($file)
{
	$nb_users = 0;

	global $DATABASE;
	$dal = new DAL("./data/$DATABASE");
	if ( ($group = $dal->select_group_by_name($file)) !== false ) {	
		if ( count($group) == 0 )
			$IDgroup = $dal->create_group($file);		
		else		
			$IDgroup = $group[0]['IDgroup'];

		if ( $IDgroup ) {
			$dir  = "./data/users";
			$data = explode("\n", file_get_contents("$dir/$file.csv"));
			foreach ($data as $items) {
				$item = explode(",", $items);
				if ( count($item) == 2 )
					if ( $dal->insert_account($IDgroup, trim($item[0]), trim($item[1])) )
						$nb_users++;
				}
			}
		}

	return $nb_users;
}
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("import.html");

$tpl->setVar('action', "import_users");
$tpl->setVar('import', $MSG_ADDUSR);
$tpl->setVar('display', "none");

if ( @$_POST["submit"] == "new" and @$_POST["checkbox"] ) {
	foreach ( @$_POST["checkbox"] as $file ) {
		$nb_users = create_database($file);

		if ( $nb_users < 0 ) {
			$tpl->setVar('icon', "block");
			$tpl->setVar('color', "red");
			$tpl->setVar('status', msgFormat($MSG_ERRIMPORT, $file));
			}
		else {
			$tpl->setVar('icon', "thumb_up");
			$tpl->setVar('color', "green");
			$tpl->setVar('status', msgFormat($MSG_IMPORTUSR, Array($file, $nb_users)));
			}

		$tpl->render('qcm');
		}
	$tpl->render('import');
	}
else {
	$count = 0;

	$dir   = "./data/users";
	foreach ( scandir($dir) as $file ) {
		if( is_file("$dir/$file") and "$dir/$file" != '.' and "$dir/$file" != '..' )
			if( substr(strtolower("$dir/$file"), -4, 4) == ".csv" ) {
				$count++;
				$tpl->setVar('file', $file);
				$tpl->setVar('value', substr($file, 0, strlen($file) - 4));
				$tpl->render('csvfile');
				}
		}
		
	if ( $count == 0 )
		$tpl->render('empty');
	}

if ( @$_SESSION["id"] == 1 )
	$tpl->render('admin');
if ( @$_SESSION["id"] )
	$tpl->render('user');

echo $tpl->render();
?>
