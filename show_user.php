<?php
/**
    @file   show_user.php
    @brief  affichage de la liste des utilisateurs

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( @$_SESSION["id"] != 1 ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./scores.php";
require "./data/msg/$LANG.php";
require "./inc/msgFormat.php";
require "./inc/dal.class.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
// connexion à la base de données
$dal = new DAL("./data/$DATABASE");
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("show_user.html");

if ( @$_GET["action"] ) {
	switch( $_GET["action"] ) {
		case "del":
			if ( @$_GET["item"] )
				$dal->delete_account(@$_GET["item"]);
			else if ( @$_GET["group"] )
				$dal->delete_group(@$_GET["group"]);
			break;
		case "edit":
			header('Location: ./add_user.php?id='.@$_GET["item"]);
			exit;
		default:
			break;
		}
	}

$IDgroup = isset($_GET["IDgroup"]) ? $_GET["IDgroup"] : 1 ;
$groups  = $dal->select_all_group();
foreach ( $groups as $g ) {
	$tpl->setVar('group', $g);
	$users = @$dal->select_all_account($g['IDgroup']);
	if ( $IDgroup == $g['IDgroup'] ) {
		$tpl->setVar('ident', "___".$g['ident']);
		$tpl->setVar('IDgroup', $g['IDgroup']);
		if ( count($users) )
			foreach ( $users as $user ) {
				$score = @$dal->select_last_mcq($user['IDuser']);				
				$tpl->setVar('score', count($score) ? get_score($score[0]['IDmcq'], $score[0]['title'])."%" : "-");
				$tpl->setVar('user', $user);
				$tpl->render('row');
				}
		else
			$tpl->render('empty');
		}
	$tpl->setVar('count', count($users));
	$tpl->render('group');
	}
	
if ( @$_SESSION["id"] == 1 )
	$tpl->render('admin');
if ( @$_SESSION["id"] )
	$tpl->render('user');

echo $tpl->render();
?>




