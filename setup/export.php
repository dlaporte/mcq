<?php
/**
    @file   export.php
    @brief  export fichiers json au format csv

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark
*/

//---------------------------------------------------------------------------
$dir = "../data";
foreach ( scandir($dir) as $file )
	if( is_file("$dir/$file") and "$dir/$file" != '.' and "$dir/$file" != '..' )
		if( substr(strtolower("$dir/$file"), -5, 5) == ".json" ) {
			$count = 0;
			$csv   = "./export/" .substr($file, 0, strlen($file) - 5). ".csv";
			@unlink($csv);
			foreach ( json_decode(file_get_contents("$dir/$file"), true) as $qcm )
				for ($i = 0; $i < count($qcm); $i++) {
					$count++;
					file_put_contents(
						$csv, 
						$qcm[$i]['question']."\t".
						$qcm[$i]['type']."\t".
						$qcm[$i]['answers'][0]['text']."\t".
						$qcm[$i]['answers'][0]['check']."\n", 
						FILE_APPEND);
					for ($j = 1; $j < count($qcm[$i]['answers']); $j++)
						file_put_contents(
							$csv, 
							"\t\t".
							$qcm[$i]['answers'][$j]['text']."\t".
							$qcm[$i]['answers'][$j]['check']."\n", 
							FILE_APPEND);
					}
			echo "$csv: $count questions<br/>";
			}

//---------------------------------------------------------------------------
?>