<?php
/**
    @file   setup.php
    @brief  initialisation de la base de données

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark
*/

//---------------------------------------------------------------------------
require "../config.php";
require "../inc/dal.class.php";
//---------------------------------------------------------------------------
chdir("../data");
if ( file_exists("$DATABASE.sqlite") ) {
	print("$DATABASE.sqlite found <br/>");
	unlink("$DATABASE.sqlite");
	}
	
// connexion à la base de données
$dal = new DAL($DATABASE);
if ( $dal->create_database() ) {
	print("create $DATABASE.sqlite <br/>");
	if ( !$dal->create_default_group() )
		print("group creation Error <br/>");
	else {
		print("create default group <br/>");
		if ( !$dal->create_admin_account() )
			print("admin account Error");
		else
			print("create admin account <br/>");
		}
	}
else
	print("$DATABASE error <br/>");
//---------------------------------------------------------------------------
?>