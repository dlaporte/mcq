<?php
/**
    @file   index.php
    @brief  page d'accueil du QCM

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
//---------------------------------------------------------------------------
require "./config.php";
require "./data/msg/$LANG.php";
require "./inc/dal.class.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
// connexion à la base de données
$dal = new DAL("./data/$DATABASE");
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("index.html");

$tpl->setVar('name', $MSG_NAME);
$tpl->setVar('fname', $MSG_FNAME);
$tpl->setVar('unknown', $MSG_UNKNOWN);
$tpl->setVar('connect', $MSG_CONNECT);

// barre navigation
switch ( (int) @$_GET["item"] ) {
	case 1:		// identification
		$tpl->setVar('display', "none");
		$tpl->render('ident');
		break;
	case 99:	// déconnexion
		$_SESSION = array();
		session_destroy();
		header('Location: ./index.php');
		exit;
	default:	// home page
		if ( @$_POST["submit"] == "ident" ) {
			$_SESSION["id"] = @$dal->select_account(@$_POST["name"], @$_POST["fname"])[0]["IDuser"];
			if ( $_SESSION["id"] == NULL ) {
				$tpl->setVar('display', "block");
				$tpl->render('ident');
				}
			else
				$tpl->render('home');
			}
		else
			$tpl->render('home');
		break;
	}

if ( @$_SESSION["id"] == 1 )
	$tpl->render('admin');
if ( @$_SESSION["id"] )
	$tpl->render('user');

echo $tpl->render();
?>