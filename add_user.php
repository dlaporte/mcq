<?php
/**
    @file   add_user.php
    @brief  gestion des utilisateurs

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( @$_SESSION["id"] != 1 ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./data/msg/$LANG.php";
require "./inc/dal.class.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
// connexion à la base de données
$dal = new DAL("./data/$DATABASE");
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("add_user.html");

$tpl->setVar('name', $MSG_NAME);
$tpl->setVar('fname', $MSG_FNAME);
$tpl->setVar('new', $MSG_NEW);
$tpl->setVar('display', "none");

if ( @$_GET["id"] )
	if ( ($user = $dal->select_account_by_id($_GET["id"])) != false ) {
		$tpl->setVar('id', $user[0]["IDuser"]);
		$tpl->setVar('uname', $user[0]["name"]);
		$tpl->setVar('ufname', $user[0]["fname"]);
		}

if ( @$_POST["submit"] == "new" ) {
	$tpl->setVar('status',  $MSG_ISOK);
	$tpl->setVar('color',   "green");
	$tpl->setVar('display', "block");
	
	if ( @$_POST["id"] ) {
		if ( !$dal->update_account(@$_POST["id"], @$_POST["name"], @$_POST["fname"]) ) {
			$tpl->setVar('status', $MSG_ERRUPDATE);
			$tpl->setVar('color', "red");
			}
		}
	else
		if ( !$dal->insert_account(1, @$_POST["name"], @$_POST["fname"]) ) {
			$tpl->setVar('status', $MSG_ERRAPPEND);
			$tpl->setVar('color', "red");
			}
	}

if ( @$_SESSION["id"] == 1 )
	$tpl->render('admin');
if ( @$_SESSION["id"] )
	$tpl->render('user');
	
echo $tpl->render();
?>