<?php
/**
    @file   quizz.php
    @brief  le QCM

    @version   1.0
    @author    profs BP SN
    @date      18/02/23
    @remark    
*/

// SPDX-License-Identifier: GPL-3.0-or-later

session_start();
			
if ( !isset($_SESSION["id"]) ) {
	header('Location: ./index.php');
	exit;
	}
//---------------------------------------------------------------------------
require "./config.php";
require "./scores.php";
require "./data/msg/$LANG.php";
require "./inc/dal.class.php";
require "./inc/hyla_tpl.class.php";
//---------------------------------------------------------------------------
// connexion à la base de données
$dal = new DAL("./data/$DATABASE");
//---------------------------------------------------------------------------
function array_to_string($array)
{
	$string = "";
	if ( isset($array) ) {
		if ( count($array) )
			$string .= $array[0];
		for ($i = 1; $i < count($array); $i++)
			$string .= ",".$array[$i];
		}
	return $string;
}
//---------------------------------------------------------------------------
function get_random($max)
{
	global $RANDOM;

	$rand = array();
	$i    = 0; 
	while ( ($i < $RANDOM) and ($i < $max) ) {
		//$nbr = random_int(1, $max);		// PHP 7+
		$nbr = rand(1, $max);		// PHP 4+
		if ( !in_array($nbr, $rand) ) {
			$rand[] = $nbr;
			$i++;
			}
		}
	return $rand;
}
//---------------------------------------------------------------------------
$tpl = new Hyla_Tpl("./tpl");
$tpl->importFile("quizz.html");

// le QCM choisi
if ( @$_GET["title"] )
	$_SESSION["title"] = $_GET["title"];

// les pages aléatoires sont tirées une seule fois
$quizz = json_decode(file_get_contents("./data/".$_SESSION["title"].".json"), true);

if ( @$_GET["id"] ) {
	//--- visualisation des réponses à un questionnaire
	$mcq = $dal->select_mcq_questions($_GET["id"]);
	if ( @$_SESSION["id"] != 1 )
		if ( $mcq[0]['IDuser'] != $_SESSION["id"] ) {
			header('Location: ./index.php?item=99');
			exit;
			}
			
	$_SESSION["random_page"] = $mcq[0]["questions"];
	$_SESSION["IDmcq"]       = $_GET["id"];
	$_SESSION["readonly"]    = true;
	}
else if ( !isset($_POST["page"]) ) {
	if ( @$_SESSION["id"] != 1 ) {
		//--- les questions sont tirées aléatoirement
		$_SESSION["random_page"] = array_to_string(get_random(count($quizz['questions'])));
		$_SESSION["IDmcq"]       = $dal->insert_mcq(@$_SESSION["id"], @$_SESSION["title"], $_SESSION["random_page"]);
		}
	//-- l'admin ne fait pas le questionnaire => il parcourt les questions avec les bonnes réponses
	$_SESSION["readonly"]    = false;
	}

// on met à jour le numéro de question
$page = @$_POST["page"] ? (int) $_POST["page"] : 1 ;
if ( isset($_POST["prev"]) or isset($_POST["next"]) ) {
	$index = ((@$_SESSION["id"] == 1) and !$_SESSION["readonly"])
		? $page - 1
		: (int) explode(",", $_SESSION["random_page"])[$page - 1] ;

	update_scores($index, @$_POST["checkbox"]);

	if ( isset($_POST["prev"]) )
		$page--;
	else
		$page++;
	}

// sélection de la question (différent du n° de page sur tirage aléatoire questions)
if ( (@$_SESSION["id"] == 1) and !$_SESSION["readonly"] ) {
	$total = count($quizz['questions']);
	$index = $page - 1;
	$image = $page;
	}
else {
	$total = count(explode(",", $_SESSION["random_page"]));
	$index = (int) @explode(",", $_SESSION["random_page"])[$page - 1] - 1;
	$image = $index + 1;
	}

$answer = $dal->select_scores_answer(@$_SESSION["IDmcq"]);	// réponses données

$tpl->setVar('button_prev', $MSG_PREV);
$tpl->setVar('button_next', $MSG_NEXT);
if ( $page == 1 )
	$tpl->setVar('left_disabled', "disabled");
else if ( $page == $total ) {
	$tpl->setVar('red', "red");
	$tpl->setVar('button_next', $MSG_END);
	}
else if ( $page > $total ) {
	header('Location: ./results.php');
	exit;
	}
$tpl->setVar('Q', $MSG_QUESTIONS[0]);
$tpl->setVar('total', $total);
$tpl->setVar('page',  $page);
$tpl->setVar('points', 
	@$_SESSION["readonly"] 
		? "[".compute_score($quizz['questions'][$index], @$answer[$page-1])." pts]" 
		: "");

// affichage question
$question = explode("|", $quizz['questions'][$index]['question']);
$tpl->setVar('question', $question[0]);

if ( count($question) > 1 ) {
	$tpl->setVar('texte', $question[1]);
	$tpl->render('texte');
	}

if ( file_exists("./data/media/".@$_SESSION["title"]."/$image.png") ) {
	$tpl->setVar('dir', $_SESSION["title"]);
	$tpl->setVar('image', $image);
	$tpl->render('image');
	}

for ($i = 0; $i < count($quizz['questions'][$index]['answers']); $i++) {
	$tpl->setVar('text', $quizz['questions'][$index]['answers'][$i]['text']);
	
	if ( @$_SESSION["id"] == 1 or $_SESSION["readonly"] ) {
		// affichage bonne réponse
		$tpl->setVar('checked', strtolower($quizz['questions'][$index]['answers'][$i]['check']) == "x" ? "checked" : "");
		$tpl->setVar('disabled', "disabled");
		
		if ( $_SESSION["readonly"] ) {
			// affichage réponse donnée à l'évaluation	
			$tpl->setVar('false', "");		
			if ( in_array($i+1, @explode(",", $answer[$page-1]['answer'])) ) {
				if ( strtolower($quizz['questions'][$index]['answers'][$i]['check']) != "x" )
					$tpl->setVar('false', "false");
				$tpl->setVar('check', "check");
				}
			else
				$tpl->setVar('check', "uncheck");
			$tpl->render('answer');
			}
		}
	else if ( $page <= count($answer) )
		if ( in_array($i+1, @explode(",", $answer[$page-1]['answer'])) )
			$tpl->setVar('checked', "checked");
		else
			$tpl->setVar('checked', "");
		
	$tpl->setVar('box', $i+1);
	$tpl->render('checkbox');
	}

if ( @$_SESSION["id"] == 1 )
	$tpl->render('admin');
if ( @$_SESSION["id"] )
	$tpl->render('user');
	
echo $tpl->render();
?>